package as.sort;

import java.util.Arrays;

public class MergeSort implements SortingAlgorithm {
    @Override
    public int[] sort(int[] input) {
        int[] inputCopy = Arrays.copyOf(input, input.length);
        return sort(inputCopy, 0, inputCopy.length - 1);
    }

    private int[] sort(int[] input, int low, int hi) {

        if (hi <= low) {
            return input;
        }
        int midPoint = low + (hi - low) / 2;

        sort(input, low, midPoint);
        sort(input, midPoint + 1, hi);
        return merge(input, low, midPoint, hi);
    }

    private int[] merge(int[] input, int low, int midPoint, int hi) {
        int[] auxArray = Arrays.copyOfRange(input, low, hi + 1);
        int leftArrayIndex = 0;
        int leftArrayRange = midPoint - low;
        int rightArrayIndex = leftArrayRange + 1;
        for (int i = low; i <= hi; i++) {
            if (leftArrayIndex > leftArrayRange) {
                input[i] = auxArray[rightArrayIndex++];
            } else if (rightArrayIndex >= auxArray.length) {
                input[i] = auxArray[leftArrayIndex++];
            } else if (auxArray[rightArrayIndex] < auxArray[leftArrayIndex]) {
                input[i] = auxArray[rightArrayIndex++];
            } else {
                input[i] = auxArray[leftArrayIndex++];
            }
        }
        return input;
    }

}
