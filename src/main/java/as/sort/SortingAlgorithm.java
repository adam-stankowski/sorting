package as.sort;

public interface SortingAlgorithm {
    int[] sort(int[] input);
}
