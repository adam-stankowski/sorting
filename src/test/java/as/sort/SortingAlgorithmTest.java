package as.sort;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SortingAlgorithmTest {

    private SortingAlgorithm sort;

    @Before
    public void setUp(){
        sort = new MergeSort();
    }

    @Test
    public void whenEmptyArrayThenEmptyReturned() {
        int[] result = sort.sort(new int[] {});
        MatcherAssert.assertThat(result.length, Matchers.is(0));
    }

    @Test
    public void whenSingleItemInArrayThenSingleItemReturned() {
        int[] result = sort.sort(new int[] { 3 });
        MatcherAssert.assertThat(result.length, Matchers.is(1));
        MatcherAssert.assertThat(result[0], Matchers.is(3));
    }

    @Test
    public void whenTwoItemsThenSorted() {
        int[] result = this.sort.sort(new int[] { 3, 1 });
        MatcherAssert.assertThat(result.length, Matchers.is(2));
        Assert.assertArrayEquals(new int[] { 1, 3 }, result);

        result = this.sort.sort(new int[] { 1, 3 });
        Assert.assertArrayEquals(new int[] { 1, 3 }, result);
    }

    @Test
    public void whenThreeItemsThenSorted() {

        int[] result = this.sort.sort(new int[] { 1, 3, 2 });
        Assert.assertArrayEquals(new int[] { 1, 2, 3 }, result);
    }

    @Test
    public void testsFourItemArray() {
        int[] result = this.sort.sort(new int[] { 5, 3, 2, 6 });
        Assert.assertArrayEquals(new int[] { 2, 3, 5, 6 }, result);

        result = this.sort.sort(new int[] { 5, 6, 7, 4 });
        Assert.assertArrayEquals(new int[] { 4, 5, 6, 7 }, result);

        result = this.sort.sort(new int[] { 1, 2, 3, 4 });
        Assert.assertArrayEquals(new int[] { 1, 2, 3, 4 }, result);
    }
}